/**
 *   В пакете by.artezio.cloud.publishing.web.rest должны распологаться контроллеры,
 *   реализующие REST сервисы, и контроллеры, возвращающие пользователю представления
 *   (jsp вьюшки).
 */
package by.artezio.cloud.publishing.web;
